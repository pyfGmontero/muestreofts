package com.pyfflowers.muestreoft.utils

object Endpoints {
    const val curvaBoton = "v2/CurvaBoton"
    const val muestra = "Muestra"
    const val muestraLista = "$muestra/Lista"
    const val empleado: String = "Empleado"
    const val planoSiembra: String = "v2/PlanoSiembra"
}
