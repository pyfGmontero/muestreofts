package com.pyfflowers.muestreoft.models


import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "planoSiembra")
data class PlanoSiembra(
    @ColumnInfo(name = "bulbos")
    @SerializedName("Bulb_x_Ciclo")
    var bulbXCiclo: Double = 0.0,
    @ColumnInfo(name = "bulbosReales")
    @SerializedName("Bulb_x_Ciclo_Real")
    var bulbXCicloReal: Double = 0.0,
    @ColumnInfo(name = "byear")
    @SerializedName("BulbYear")
    var bulbYear: Int = 0,
    @ColumnInfo(name = "cama")
    @SerializedName("Cama")
    var cama: String = "",
    @ColumnInfo(name = "ciclo")
    @SerializedName("Ciclo")
    var ciclo: Int = 0,
    @ColumnInfo(name = "color")
    @SerializedName("Color")
    var color: String = "",
    @ColumnInfo(name = "comun")
    @SerializedName("Comun")
    var comun: String = "",
    @ColumnInfo(name = "country")
    @SerializedName("Country")
    var country: String = "",
    @ColumnInfo(name = "dia")
    @SerializedName("Dia")
    var dia: String = "",
    @ColumnInfo(name = "finca")
    @SerializedName("Finca")
    var finca: String = "",
    @PrimaryKey
    @ColumnInfo(name = "id")
    @SerializedName("ID_Plano")
    var id: Int = 0,
    @ColumnInfo(name = "inv")
    @SerializedName("Inv")
    var inv: String = "",
    @ColumnInfo(name = "lote")
    @SerializedName("Lot")
    var lote: String = "",
    @ColumnInfo(name = "proov")
    @SerializedName("Proov")
    var proov: String = "",
    @ColumnInfo(name = "recov")
    @SerializedName("Recov")
    var recov: Double = 0.0,
    @ColumnInfo(name = "scWeek")
    @SerializedName("SC_Week")
    var sCWeek: Int = 0,
    @ColumnInfo(name = "scYear")
    @SerializedName("SC_Year")
    var sCYear: Int = 0,
    @ColumnInfo(name = "size")
    @SerializedName("SIZE")
    var size: String = "",
    @ColumnInfo(name = "ssWeek")
    @SerializedName("SS_Week")
    var sSWeek: Int = 0,
    @ColumnInfo(name = "ssYear")
    @SerializedName("SS_Year")
    var sSYear: Int = 0,
    @ColumnInfo(name = "variedad")
    @SerializedName("Vari")
    var variedad: String = ""
):IBaseModel