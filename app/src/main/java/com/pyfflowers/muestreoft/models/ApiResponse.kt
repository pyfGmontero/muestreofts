package com.pyfflowers.muestreoft.models


import com.google.gson.annotations.SerializedName

data class ApiResponse<T>(
    @SerializedName("Data")
    var data: ArrayList<T> = ArrayList(),
    @SerializedName("Message")
    var message: String = ""
)