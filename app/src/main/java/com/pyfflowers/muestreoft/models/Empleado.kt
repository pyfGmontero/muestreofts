package com.pyfflowers.muestreoft.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Empleado ():Parcelable{
    @SerializedName("EmpleadoId")
    @Expose
    var empleadoId: Int? = null
    @SerializedName("Identificacion")
    @Expose
    var identificacion: String? = null
    @SerializedName("Codigo")
    @Expose
    var codigo: String? = null
    @SerializedName("PrimerApellido")
    @Expose
    var primerApellido: String? = null
    @SerializedName("SegApellido")
    @Expose
    var segApellido: String? = null
    @SerializedName("Nombre")
    @Expose
    var nombre: String? = null
    @SerializedName("PlantaId")
    @Expose
    var plantaId: Int? = null
    @SerializedName("FincaId")
    @Expose
    var fincaId: Int? = null
    @SerializedName("PuestoId")
    @Expose
    var puestoId: Int? = null
    @SerializedName("Activo")
    @Expose
    var activo: Int? = null

    constructor(parcel: Parcel) : this() {
        empleadoId = parcel.readValue(Int::class.java.classLoader) as? Int
        identificacion = parcel.readString()
        codigo = parcel.readString()
        primerApellido = parcel.readString()
        segApellido = parcel.readString()
        nombre = parcel.readString()
        plantaId = parcel.readValue(Int::class.java.classLoader) as? Int
        fincaId = parcel.readValue(Int::class.java.classLoader) as? Int
        puestoId = parcel.readValue(Int::class.java.classLoader) as? Int
        activo = parcel.readValue(Int::class.java.classLoader) as? Int
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(empleadoId)
        parcel.writeString(identificacion)
        parcel.writeString(codigo)
        parcel.writeString(primerApellido)
        parcel.writeString(segApellido)
        parcel.writeString(nombre)
        parcel.writeValue(plantaId)
        parcel.writeValue(fincaId)
        parcel.writeValue(puestoId)
        parcel.writeValue(activo)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Empleado> {
        override fun createFromParcel(parcel: Parcel): Empleado {
            return Empleado(parcel)
        }

        override fun newArray(size: Int): Array<Empleado?> {
            return arrayOfNulls(size)
        }
    }
}

