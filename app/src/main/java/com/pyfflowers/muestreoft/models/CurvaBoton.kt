package com.pyfflowers.muestreoft.models


import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "curvas")
data class CurvaBoton(
    @SerializedName("Anno")
    @ColumnInfo(name = "anno")
    var anno: Int = 0,
    @SerializedName("Calibre")
    @ColumnInfo(name = "calibre")
    var calibre: String = "",
    @SerializedName("Dia")
    @ColumnInfo(name = "dia")
    var dia: Int = 0,
    @SerializedName("Finca")
    @ColumnInfo(name = "finca")
    var finca: String = "",
    @SerializedName("Valor")
    @ColumnInfo(name = "valor")
    var valor: Double = 0.0,
    @SerializedName("Variedad")
    @ColumnInfo(name = "variedad")
    var variedad: String = ""
) {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Int = 0
}