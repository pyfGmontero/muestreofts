package com.pyfflowers.muestreoft.models


import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "muestras")
data class Muestra(
    @ColumnInfo(name = "bulbosSembrados")
    @SerializedName("BulbosSembrados")
    var bulbosSembrados: Int = 0,
    @ColumnInfo(name = "byear")
    @SerializedName("Byear")
    var byear: Int = 0,
    @ColumnInfo(name = "calibre")
    @SerializedName("Calibre")
    var calibre: String = "",
    @ColumnInfo(name = "cama")
    @SerializedName("CamaSiembra")
    var camaSiembra: String = "",
    @ColumnInfo(name = "clase")
    @SerializedName("Clase")
    var clase: String = "",
    @ColumnInfo(name = "diaMuestra")
    @SerializedName("DiaMuestreo")
    var diaMuestreo: String = "",
    @ColumnInfo(name = "diaCosecha")
    @SerializedName("DiasCosecha")
    var diasCosecha: Int = 0,
    @ColumnInfo(name = "estimador")
    @SerializedName("Estimador")
    var estimador: String = "",
    @ColumnInfo(name = "finca")
    @SerializedName("Finca")
    var finca: String = "",
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    @SerializedName("Id")
    var id: Int = 0,
    @ColumnInfo(name = "invernadero")
    @SerializedName("Invernadero")
    var invernadero: String = "",
    @ColumnInfo(name = "li")
    @SerializedName("Li")
    var li: Double = 0.0,
    @ColumnInfo(name = "lote")
    @SerializedName("Lote")
    var lote: String = "",
    @ColumnInfo(name = "ls")
    @SerializedName("Ls")
    var ls: Double = 0.0,
    @ColumnInfo(name = "medidaBoton")
    @SerializedName("MedidaBoton")
    var medidaBoton: Double = 0.0,
    @ColumnInfo(name = "proveedor")
    @SerializedName("Proveedor")
    var proveedor: String = "",
    @ColumnInfo(name = "semanaCosecha")
    @SerializedName("SCosecha")
    var sCosecha: Int = 0,
    @ColumnInfo(name = "semanaEstimada")
    @SerializedName("SEstimada")
    var sEstimada: Int = 0,
    @ColumnInfo(name = "variedad")
    @SerializedName("Variedad")
    var variedad: String = "",
    @ColumnInfo(name = "zona")
    @SerializedName("Zona")
    var zona: String = ""
)