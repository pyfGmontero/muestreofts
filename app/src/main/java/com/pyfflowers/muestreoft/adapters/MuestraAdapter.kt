package com.pyfflowers.muestreoft.adapters

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import com.pyfflowers.muestreoft.R
import com.pyfflowers.muestreoft.models.Muestra
import kotlinx.android.synthetic.main.muestra_list_item.view.*


class MuestraAdapter(private val interaction: Interaction? = null) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Muestra>() {

        override fun areItemsTheSame(oldItem: Muestra, newItem: Muestra): Boolean {
            return oldItem.id==newItem.id
        }

        override fun areContentsTheSame(oldItem: Muestra, newItem: Muestra): Boolean {
            return oldItem==newItem
        }

    }
    private val differ = AsyncListDiffer(this, DIFF_CALLBACK)


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return MuestraViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.muestra_list_item,
                parent,
                false
            ),
            interaction
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is MuestraViewHolder -> {
                holder.bind(differ.currentList.get(position))
            }
        }
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    fun submitList(list: List<Muestra>) {
        differ.submitList(list)
    }

    class MuestraViewHolder
    constructor(
        itemView: View,
        private val interaction: Interaction?
    ) : RecyclerView.ViewHolder(itemView) {

        fun bind(item: Muestra) = with(itemView) {
            txt_dia.text=item.diasCosecha.toString()
            txt_medida.text=item.medidaBoton.toString()
            txt_invernadero.text=item.invernadero
            txt_finca.text=item.finca
            txt_variedad.text=item.variedad
            txt_lote.text=item.lote
            itemView.setOnClickListener {
                interaction?.onItemSelected(adapterPosition, item)
            }
        }
    }

    interface Interaction {
        fun onItemSelected(position: Int, item: Muestra)
    }
}
