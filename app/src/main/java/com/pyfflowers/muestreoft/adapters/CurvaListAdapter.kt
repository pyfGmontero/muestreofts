package com.pyfflowers.muestreoft.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.pyfflowers.muestreoft.R
import com.pyfflowers.muestreoft.models.CurvaBoton
import kotlinx.android.synthetic.main.curva_list.view.*

class CurvaListAdapter(private val interaction: Interaction? = null) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<CurvaBoton>() {

        override fun areItemsTheSame(oldItem: CurvaBoton, newItem: CurvaBoton): Boolean =
            oldItem.id == newItem.id


        override fun areContentsTheSame(oldItem: CurvaBoton, newItem: CurvaBoton): Boolean =
            oldItem == newItem


    }
    private val differ = AsyncListDiffer(this, DIFF_CALLBACK)


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return CurvaListHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.curva_list,
                parent,
                false
            ),
            interaction
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is CurvaListHolder -> {
                holder.bind(differ.currentList[position])
            }
        }
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    fun submitList(list: List<CurvaBoton>) {
        differ.submitList(list)
    }

    class CurvaListHolder
    constructor(
        itemView: View,
        private val interaction: Interaction?
    ) : RecyclerView.ViewHolder(itemView) {

        fun bind(item: CurvaBoton) = with(itemView) {
            setOnClickListener {
                interaction?.onItemSelected(adapterPosition, item)
            }
            with(item) {
                txt_anno.text = anno.toString()
                txt_calibre.text = calibre
                txt_dia.text = dia.toString()
                txt_finca.text = finca
                txt_valor.text = valor.toString()
                txt_variedad.text = variedad
            }

        }
    }

    interface Interaction {
        fun onItemSelected(position: Int, item: CurvaBoton)
    }
}
