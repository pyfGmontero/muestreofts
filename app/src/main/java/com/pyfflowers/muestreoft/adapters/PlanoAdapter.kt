package com.pyfflowers.muestreoft.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.pyfflowers.muestreoft.R
import com.pyfflowers.muestreoft.models.PlanoSiembra

class PlanoAdapter(private val interaction: Interaction? = null) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val _diffCallback = object : DiffUtil.ItemCallback<PlanoSiembra>() {

        override fun areItemsTheSame(oldItem: PlanoSiembra, newItem: PlanoSiembra): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: PlanoSiembra, newItem: PlanoSiembra): Boolean {
            return oldItem == newItem
        }

    }
    private val differ = AsyncListDiffer(this, _diffCallback)


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return PlanoSiembraViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.plano_list_holder,
                parent,
                false
            ),
            interaction
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is PlanoSiembraViewHolder -> {
                holder.bind(differ.currentList[position])
            }
        }
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    fun submitList(list: List<PlanoSiembra>) {
        differ.submitList(list)
    }

    class PlanoSiembraViewHolder
    constructor(
        itemView: View,
        private val interaction: Interaction?
    ) : RecyclerView.ViewHolder(itemView) {
        private val inv: TextView = itemView.findViewById(R.id.txt_invernadero)
        private val comun: TextView = itemView.findViewById(R.id.txt_comun)
        private val variedad: TextView = itemView.findViewById(R.id.txt_variedad)
        private val calibre: TextView = itemView.findViewById(R.id.txt_calibre)
        private val lote: TextView = itemView.findViewById(R.id.txt_lote)
        private val finca: TextView = itemView.findViewById(R.id.txt_finca)
        private val cosecha: TextView = itemView.findViewById(R.id.txt_cosecha)
        private val siembra: TextView = itemView.findViewById(R.id.txt_siembra)
        fun bind(item: PlanoSiembra) = with(itemView) {
            inv.text = item.inv
            comun.text = item.comun
            variedad.text = item.variedad
            calibre.text = item.size
            lote.text = item.lote
            finca.text = item.finca
            cosecha.text=item.sCWeek.toString()
            siembra.text=item.sSWeek.toString()
            itemView.setOnClickListener {
                interaction?.onItemSelected(adapterPosition, item)
            }
        }
    }

    interface Interaction {
        fun onItemSelected(position: Int, item: PlanoSiembra)
    }
}
