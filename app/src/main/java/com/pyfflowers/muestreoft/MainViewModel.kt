package com.pyfflowers.muestreoft

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.pyfflowers.muestreoft.models.Muestra
import com.pyfflowers.muestreoft.models.PlanoSiembra
import com.pyfflowers.muestreoft.repositories.MainRepository

class MainViewModel(application: Application) : AndroidViewModel(application) {

    private val mainRepository by lazy {
        MainRepository(application)
    }
    private val _curvas: MutableLiveData<String> = MutableLiveData()
    private val _invernadero: MutableLiveData<PlanoSiembra> =
        MutableLiveData<PlanoSiembra>().apply {
            value = PlanoSiembra()
        }
    private val _target: MutableLiveData<PlanoSiembra> = MutableLiveData()
    val target: LiveData<PlanoSiembra>
        get() = _target
    val invernadero: LiveData<PlanoSiembra>
        get() = _invernadero
    val planos: LiveData<List<PlanoSiembra>> = Transformations.switchMap(_invernadero) {
        mainRepository.getPlano(it.finca, it.inv, it.comun, it.variedad, it.lote, it.size)
    }
    private val _cama: MutableLiveData<Muestra> = MutableLiveData<Muestra>().apply {
        value = Muestra()
    }
    val muestras = Transformations.switchMap(_cama) { muestra ->
        mainRepository.fetchMuestras(muestra)
    }
    val totales = Transformations.switchMap(invernadero) { muestra ->

        with(invernadero.value!!){
        mainRepository.getMuestras(finca,inv,lote,variedad,size)

        }
    }
    val cama: LiveData<Muestra>
        get() = _cama

    fun setInvernadero(planoSiembra: PlanoSiembra) {
        _invernadero.postValue(planoSiembra)
    }

    fun setCama(muestra: Muestra) {
        _cama.postValue(muestra)
    }



    fun fetchPlano() {
        mainRepository.fetchPlano()
    }

    fun setTarget(item: PlanoSiembra) {
        _target.postValue(item)
    }

    fun insertMuestra(vararg muestra: Muestra) {
//        _cama.postValue(cama.value)
        mainRepository.insertMuestra(*muestra)
    }
    fun subriMuestras(){
        mainRepository.postMuestras(totales.value)
    }


}