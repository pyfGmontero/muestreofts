package com.pyfflowers.muestreoft.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.pyfflowers.muestreoft.models.Muestra

@Dao
interface IMuestraDao : IBaseDao<Muestra> {
    @Query("select * from muestras")
    fun get(): LiveData<List<Muestra>>

    @Query("select * from muestras where invernadero =:inv and cama=:cama and variedad=:variedad and lote=:lote and calibre=:size and diaCosecha=:dia and finca=:finca and semanaCosecha=:cosecha")
    fun get(
        inv: String,
        variedad: String,
        lote: String,
        size: String,
        dia: Int,
        finca: String,
        cosecha: Int,
        cama: String
    ): LiveData<List<Muestra>>

    @Query("Delete from muestras")
    fun delete(): Int

    @Query("select * from muestras where finca like :finca||'%' and invernadero like :inv||'%' and lote like :lote||'%' and variedad like :variedad||'%' and calibre like :calibre||'%'")
    fun get(
        finca: String,
        inv: String,
        lote: String,
        variedad: String,
        calibre: String
    ): LiveData<List<Muestra>>
}