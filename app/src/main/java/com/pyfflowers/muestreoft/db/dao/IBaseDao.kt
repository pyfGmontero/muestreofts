package com.pyfflowers.muestreoft.db.dao

import androidx.room.*

interface IBaseDao<T> {
    @Insert
    fun insert(vararg element: T): Array<Long>

    @Update
    fun update(vararg element: T): Int

    @Delete
    fun delete(vararg element: T): Int

}