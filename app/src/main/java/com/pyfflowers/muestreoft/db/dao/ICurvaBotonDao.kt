package com.pyfflowers.muestreoft.db.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.pyfflowers.muestreoft.models.CurvaBoton

@Dao
interface ICurvaBotonDao:IBaseDao<CurvaBoton> {
    @Query("Select * from curvas")
    fun getCurvaBoton(): LiveData<List<CurvaBoton>>

    @Query("Select * from curvas where dia = :dia")
    fun getCurvaBoton(dia: Int): LiveData<List<CurvaBoton>>

    @Query("Delete from curvas")
    fun deleteAll(): Int

}