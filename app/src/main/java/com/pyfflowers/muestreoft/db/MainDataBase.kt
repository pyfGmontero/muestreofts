package com.pyfflowers.muestreoft.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.pyfflowers.muestreoft.db.dao.ICurvaBotonDao
import com.pyfflowers.muestreoft.db.dao.IMuestraDao
import com.pyfflowers.muestreoft.db.dao.IPlanoSiembraDao
import com.pyfflowers.muestreoft.models.CurvaBoton
import com.pyfflowers.muestreoft.models.Muestra
import com.pyfflowers.muestreoft.models.PlanoSiembra


@Database(entities = [CurvaBoton::class, Muestra::class, PlanoSiembra::class], version = 1)
abstract class MainDataBase : RoomDatabase() {

    abstract fun curvaDao(): ICurvaBotonDao
    abstract fun muestraDao(): IMuestraDao
    abstract fun planoDao(): IPlanoSiembraDao


    companion object {
        @Volatile
        private var instance: MainDataBase? = null
        private val LOCK = Any()

        operator fun invoke(context: Context) = instance ?: synchronized(LOCK) {
            instance ?: buildDatabase(context).also { instance = it }
        }

        private fun buildDatabase(context: Context) = Room.databaseBuilder(
            context,
            MainDataBase::class.java, "papa.db"
        ) .build()
    }

}