package com.pyfflowers.muestreoft.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.pyfflowers.muestreoft.models.PlanoSiembra

@Dao
interface IPlanoSiembraDao : IBaseDao<PlanoSiembra> {
    @Query("select * from planoSiembra ")
    fun get(): LiveData<List<PlanoSiembra>>

    @Query("Delete from planoSiembra")
    fun deleteAll(): Int

    @Query("select * from planoSiembra where finca like :finca||'%' and inv like :invernadero||'%' and comun like :comun||'%' and variedad like :variedad||'%' and size like :calibre||'%' and lote like :lote||'%' ")
    fun get(
        finca: String,
        invernadero: String,
        comun: String,
        lote: String,
        calibre: String,
        variedad: String
    ): LiveData<List<PlanoSiembra>>
}