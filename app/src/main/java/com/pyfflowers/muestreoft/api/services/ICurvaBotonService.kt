package com.pyfflowers.muestreoft.api.services

import com.pyfflowers.muestreoft.models.ApiResponse
import com.pyfflowers.muestreoft.models.CurvaBoton
import com.pyfflowers.muestreoft.utils.Endpoints.curvaBoton
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.PUT

interface ICurvaBotonService {

    @GET(curvaBoton)
    suspend fun getCurvaBoton(): ApiResponse<CurvaBoton>

    @POST(curvaBoton)
    suspend fun postCurvaBoton(): ApiResponse<CurvaBoton>

    @PUT(curvaBoton)
    suspend fun putCurvaBoton(): ApiResponse<CurvaBoton>

    @DELETE(curvaBoton)
    suspend fun deleteCurvaBoton(): ApiResponse<CurvaBoton>

}