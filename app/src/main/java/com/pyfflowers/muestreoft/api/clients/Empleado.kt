package com.pyfflowers.muestreoft.api.clients

import com.pyfflowers.muestreoft.api.services.IEmpleadoService

object Empleado {
    val service: IEmpleadoService = ApiClient.buildService(IEmpleadoService::class.java)


}

val ApiClient.sEmpleado: IEmpleadoService
    get() {
        return Empleado.service
    }
