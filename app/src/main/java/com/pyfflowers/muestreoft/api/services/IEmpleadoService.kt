package com.pyfflowers.muestreoft.api.services

import com.pyfflowers.muestreoft.models.ApiResponse
import com.pyfflowers.muestreoft.models.Empleado
import com.pyfflowers.muestreoft.utils.Endpoints
import retrofit2.http.GET

interface IEmpleadoService {

    @GET(Endpoints.empleado)
    suspend fun getEmpleados(): ApiResponse<Empleado>
}