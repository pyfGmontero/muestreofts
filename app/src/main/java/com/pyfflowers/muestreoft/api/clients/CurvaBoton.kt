package com.pyfflowers.muestreoft.api.clients

import com.pyfflowers.muestreoft.api.services.ICurvaBotonService

object CurvaBoton {
    val service: ICurvaBotonService = ApiClient.buildService(ICurvaBotonService::class.java)
}

val ApiClient.sCurvaBoton: ICurvaBotonService
    get() {
        return CurvaBoton.service
    }
