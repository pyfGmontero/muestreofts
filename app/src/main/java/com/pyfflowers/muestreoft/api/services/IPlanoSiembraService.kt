package com.pyfflowers.muestreoft.api.services

import com.pyfflowers.muestreoft.models.ApiResponse
import com.pyfflowers.muestreoft.models.PlanoSiembra
import com.pyfflowers.muestreoft.utils.Endpoints
import retrofit2.http.GET


interface IPlanoSiembraService {
    @GET(Endpoints.planoSiembra)
    suspend fun getCurvaBoton(): ApiResponse<PlanoSiembra>


}