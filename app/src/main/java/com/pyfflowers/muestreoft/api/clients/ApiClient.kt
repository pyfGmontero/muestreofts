package com.pyfflowers.muestreoft.api.clients

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ApiClient {
    private const val BASE_URL = "http://192.168.200.105/estimacion/api/"
    private val retrofit: Retrofit =
        Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BASE_URL)
            .build()

    fun <T> buildService(service: Class<T>): T {
        return retrofit.create(service)
    }
}