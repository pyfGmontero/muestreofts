package com.pyfflowers.muestreoft.api.services

import com.pyfflowers.muestreoft.models.ApiResponse
import com.pyfflowers.muestreoft.models.Muestra
import com.pyfflowers.muestreoft.utils.Endpoints.muestra
import com.pyfflowers.muestreoft.utils.Endpoints.muestraLista
import retrofit2.http.*

interface IMuestraService {

    @GET(muestra)
    suspend fun getMuestra(): ApiResponse<Muestra>
    @POST(muestraLista)
    suspend fun postMuestra(@Body item: List<Muestra>): ApiResponse<Muestra>
    @PUT(muestra)
    suspend fun putMuestra(@Body item: Muestra): ApiResponse<Muestra>
    @DELETE(muestra)
    suspend fun deleteMuestra(@Body item: Muestra): ApiResponse<Muestra>

}