package com.pyfflowers.muestreoft.api.clients

import com.pyfflowers.muestreoft.api.services.IMuestraService

object Muestra {
    val service: IMuestraService = ApiClient.buildService(IMuestraService::class.java)
}

val ApiClient.sMuestra: IMuestraService
    get() {
        return Muestra.service
    }
