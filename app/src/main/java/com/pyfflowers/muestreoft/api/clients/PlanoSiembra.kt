package com.pyfflowers.muestreoft.api.clients

import com.pyfflowers.muestreoft.api.services.IPlanoSiembraService

object PlanoSiembra {
    val service: IPlanoSiembraService = ApiClient.buildService(IPlanoSiembraService::class.java)


}

val ApiClient.sPlanoSiembra: IPlanoSiembraService
    get() {
        return PlanoSiembra.service
    }
