package com.pyfflowers.muestreoft.repositories

import android.app.Application
import android.util.Log
import androidx.lifecycle.LiveData
import com.pyfflowers.muestreoft.api.clients.*
import com.pyfflowers.muestreoft.db.MainDataBase
import com.pyfflowers.muestreoft.db.dao.ICurvaBotonDao
import com.pyfflowers.muestreoft.db.dao.IMuestraDao
import com.pyfflowers.muestreoft.db.dao.IPlanoSiembraDao
import com.pyfflowers.muestreoft.models.CurvaBoton
import com.pyfflowers.muestreoft.models.Muestra
import com.pyfflowers.muestreoft.models.PlanoSiembra
import kotlinx.coroutines.CompletableJob
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import retrofit2.HttpException

class MainRepository(application: Application) {

    private var curvaDao: ICurvaBotonDao?
    private var muestraDao: IMuestraDao?
    private var planoDao: IPlanoSiembraDao?
    private var job: CompletableJob? = null

    init {
        val db = MainDataBase.invoke(application)
        curvaDao = db.curvaDao()
        muestraDao = db.muestraDao()
        planoDao = db.planoDao()
    }

    fun getCurvas(): LiveData<List<CurvaBoton>> {
        return curvaDao!!.getCurvaBoton()
    }

    fun insertCurva(vararg curvaBoton: CurvaBoton) {
        job = Job()
        job?.let { completableJob: CompletableJob ->
            CoroutineScope(IO + completableJob).launch {

                val insetCurvaBonton = curvaDao!!.insert(*curvaBoton)

                completableJob.complete()

            }
        }
    }

    fun deleteCurva(vararg curvaBoton: CurvaBoton) {
        job = Job()
        job?.let { completableJob: CompletableJob ->
            CoroutineScope(IO + completableJob).launch {
                val deleteCurvaBonton = curvaDao?.delete(*curvaBoton)
                completableJob.complete()
            }
        }
    }

    fun updateCurva(vararg curvaBoton: CurvaBoton) {
        job = Job()
        job?.let { completableJob: CompletableJob ->
            CoroutineScope(IO + completableJob).launch {
                val updateCurvaBonton = curvaDao?.update(*curvaBoton)
                completableJob.complete()
            }
        }
    }

    fun fetchCurvas() {
        job = Job()
        job?.let { completableJob: CompletableJob ->
            CoroutineScope(IO + completableJob).launch {
                try {
                    val fetchCurvaBonton = ApiClient.sCurvaBoton.getCurvaBoton()
                    if (fetchCurvaBonton.data.isNotEmpty()) {
                        val curvas = fetchCurvaBonton.data.toTypedArray()
                        val del = curvaDao!!.deleteAll()
                        val insert = curvaDao!!.insert(*curvas)
                        if (insert.isNotEmpty()) {
                            Log.i("inserciones", insert.size.toString())
                            Log.i("eliminaciones", del.toString())

                        } else {
                            Log.e("Inserciones", "Ninguna inserción")
                        }
                    }
                } catch (e: Exception) {
                    val ex = e
                }

                completableJob.complete()
            }
        }
    }

    fun getPlano(
        finca: String,
        invernadero: String,
        comun: String,
        variedad: String,
        lote: String,
        calibre: String
    ): LiveData<List<PlanoSiembra>> {
        return planoDao!!.get(finca, invernadero, comun, lote, calibre, variedad)
    }

    fun fetchPlano() {
        job = Job()
        job?.let { completableJob: CompletableJob ->
            CoroutineScope(IO + completableJob).launch {
                try {
                    val response = ApiClient.sPlanoSiembra.getCurvaBoton()
                    if (response.data.isNotEmpty()) {
                        val curvas = response.data.toTypedArray()
                        val del = planoDao!!.deleteAll()
                        val insert = planoDao!!.insert(*curvas)
                        if (insert.isNotEmpty()) {
                            Log.i("Inserciones", insert.size.toString())
                            Log.i("Eliminaciones", del.toString())
                        } else {
                            Log.e("Inserciones", "Ninguna inserción")
                        }
                    }
                } catch (e: Exception) {
                    val ex = e
                }
                completableJob.complete()
            }
        }
    }


    fun fetchEmpleados() {
        job = Job()
        job?.let { completableJob: CompletableJob ->
            CoroutineScope(IO + completableJob).launch {
                val fetchCurvaBonton = ApiClient.sEmpleado.getEmpleados()
                if (fetchCurvaBonton.data.isNotEmpty()) {
                    Log.i("MainRepository", "${fetchCurvaBonton.data.size} elements")
                }
                completableJob.complete()
            }
        }
    }

    fun insertMuestra(vararg muestra: Muestra) {
        job = Job()
        job?.let { completableJob ->
            CoroutineScope(IO + completableJob).launch {
                muestraDao?.insert(*muestra)
                completableJob.complete()
            }
        }

    }

    fun fetchMuestras(muestra: Muestra): LiveData<List<Muestra>> {
        with(muestra) {
            return muestraDao!!.get(
                invernadero,
                variedad,
                lote,
                calibre,
                diasCosecha,
                finca,
                sCosecha,
                camaSiembra
            )
        }
    }

    fun postMuestras(value: List<Muestra>?) {
        try {
            if (value== null) {
                return
            }
            job = Job()
            job?.let { completableJob: CompletableJob ->
                CoroutineScope(IO + completableJob).launch {
                    if (value.isNotEmpty()) {
                        ApiClient.sMuestra.postMuestra(value)
                        val deletes = muestraDao!!.delete(*value.toTypedArray())
                        Log.i("Deletes", deletes.toString())
                    }
                }
            }
        } catch (e: HttpException) {
            Log.e("postMuestras", e.toString())
        } finally {

        }
    }

    fun getMuestras(): LiveData<List<Muestra>> {
        return muestraDao!!.get()
    }

    fun getMuestras(
        finca: String,
        inv: String,
        lote: String,
        variedad: String,
        calibre: String
    ): LiveData<List<Muestra>> {
        val muestras = muestraDao!!.get(finca, inv, lote, variedad, calibre)
        return muestras
    }
}