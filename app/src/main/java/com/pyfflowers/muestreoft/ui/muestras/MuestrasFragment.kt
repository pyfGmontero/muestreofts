package com.pyfflowers.muestreoft.ui.muestras

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.pyfflowers.muestreoft.MainViewModel
import com.pyfflowers.muestreoft.R
import com.pyfflowers.muestreoft.adapters.MuestraAdapter
import com.pyfflowers.muestreoft.models.Muestra
import kotlinx.android.synthetic.main.fragment_muestras.*

/**
 * A simple [Fragment] subclass.
 */
class MuestrasFragment : Fragment() {

    private lateinit var mainViewModel: MainViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mainViewModel =
            ViewModelProvider(activity!!).get(MainViewModel::class.java)
        // Inflate the layout for this fragment
        val root = inflater.inflate(R.layout.fragment_muestras, container, false)
        initializeComponents(root)
        return root
    }

    private fun initializeComponents(root: View) {
//        val interactions = object : MuestraAdapter.Interaction {
//            override fun onItemSelected(position: Int, item: Muestra) {
//                Toast.makeText(context!!, item.toString(), Toast.LENGTH_SHORT).show()
//            }
//
//        }
        val adapter = MuestraAdapter()
        var recycler: RecyclerView
        var btnAnotar: Button
        var txtDia: EditText
        var txtCama: EditText
        var txtCantidad: EditText
        var txtMedida: EditText
        with(root) {
            recycler = findViewById(R.id.recycler_muestras)
            btnAnotar = findViewById(R.id.btn_anotar)
            txtDia = findViewById(R.id.txt_dia)
            txtCama = findViewById(R.id.txt_cama)
            txtCantidad = findViewById(R.id.txt_cantidad)
            txtMedida = findViewById(R.id.txt_medida)
        }
        btnAnotar.setOnClickListener {
            if (!txtCama.text.isNullOrBlank() && !txtCantidad.text.isNullOrBlank() && !txtDia.text.isNullOrBlank() && !txtMedida.text.isNullOrBlank()) {
                val muestra = mainViewModel.cama.value
                if (muestra != null) {
                    mainViewModel.insertMuestra(muestra)

                }
            }
        }
        textWatcher(txtDia) { muestra, charSequence ->
            muestra.diasCosecha = when {
                charSequence.toString().isNotBlank() -> charSequence.toString().toInt()
                else -> 0
            }
            mainViewModel.setCama(muestra)
        }
        textWatcher(txtCama) { muestra, charSequence ->
            muestra.camaSiembra =when {
                charSequence.toString().isNotBlank() -> charSequence.toString()
                else -> ""
            }
            mainViewModel.setCama(muestra)
        }
        textWatcher(txtCantidad) { muestra, charSequence ->
            muestra.bulbosSembrados = when {
                charSequence.toString().isNotBlank() -> charSequence.toString().toInt()
                else -> 0
            }
            mainViewModel.setCama(muestra)
        }
        textWatcher(txtMedida) { muestra, charSequence ->
            muestra.medidaBoton = when {
                charSequence.toString().isNotBlank() -> charSequence.toString().toDouble()
                else -> 0.0
            }
            mainViewModel.setCama(muestra)
        }
        recycler.layoutManager = GridLayoutManager(context, 2)
        recycler.adapter = adapter
        mainViewModel.target.observe(this, Observer { t ->
            txt_invernadero.text = t.inv
            txt_comun.text = t.comun
            txt_variedad.text = t.variedad
            txt_lote.text = t.lote
            txt_finca.text = t.finca
            txt_calibre.text = t.size
            txt_cosecha.text = t.sCWeek.toString()
            txt_siembra.text = t.sSWeek.toString()
            val cama = mainViewModel.cama.value
            if (cama != null) {
                cama.invernadero = t.inv
                cama.variedad = t.variedad
                cama.calibre = t.size
                cama.finca = t.finca
                cama.lote = t.lote
                cama.sCosecha = t.sCWeek
                cama.proveedor = t.proov
                cama.byear = t.bulbYear
                mainViewModel.setCama(cama)
            }
        })
        mainViewModel.muestras.observe(this, Observer { t ->
            adapter.submitList(t)
            when {
                t.isNullOrEmpty() -> {
                    btnAnotar.isVisible = true
                }
                t.size >= 15 || t.size >= 2 && t[0].bulbosSembrados <= 200 -> btnAnotar.isVisible =
                    false
                else -> btnAnotar.isVisible = true
            }

        })

    }

    private fun textWatcher(
        editText: EditText,
        function1: (Muestra, CharSequence?) -> Unit
    ) {
        val watcher = object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                val inv = mainViewModel.cama.value
                if (inv != null) {
                    function1(inv, s)
                }

            }

        }
        editText.addTextChangedListener(watcher)
    }


}
