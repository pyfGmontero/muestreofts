package com.pyfflowers.muestreoft.ui.home

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.pyfflowers.muestreoft.MainViewModel
import com.pyfflowers.muestreoft.R
import com.pyfflowers.muestreoft.adapters.PlanoAdapter
import com.pyfflowers.muestreoft.models.PlanoSiembra

class InvernaderoFragment : Fragment() {

    private lateinit var mainViewModel: MainViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mainViewModel =
            ViewModelProvider(activity!!).get(MainViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_home, container, false)
        val recyclerCurvas = root.findViewById<RecyclerView>(R.id.recyclerCurvas)
        val btnFetch = root.findViewById<Button>(R.id.btnFetch)
        val editFinca: EditText = root.findViewById(R.id.txt_finca)
        val editInv: EditText = root.findViewById(R.id.txt_inv)
        val editComun: EditText = root.findViewById(R.id.txt_comun)
        val editLote: EditText = root.findViewById(R.id.txt_lote)
        val editVariedad: EditText = root.findViewById(R.id.txt_variedad)
        val editCalibre: EditText = root.findViewById(R.id.txt_calibre)
        initializeComponents(
            recyclerCurvas,
            editFinca,
            editInv,
            editComun,
            editLote,
            editVariedad,
            editCalibre,
            btnFetch
        )

        return root
    }

    private fun initializeComponents(
        recyclerCurvas: RecyclerView,
        editFinca: EditText,
        editInv: EditText,
        editComun: EditText,
        editLote: EditText,
        editVariedad: EditText,
        editCalibre: EditText,
        btnFetch: Button
    ) {
        val interaction= object :PlanoAdapter.Interaction{
            override fun onItemSelected(position: Int, item: PlanoSiembra) {
                findNavController().navigate(R.id.inv_muestras)
                mainViewModel.setTarget(item)
            }
        }
        recyclerCurvas.layoutManager = GridLayoutManager(context, 3)
        val adapter = PlanoAdapter(interaction)
        recyclerCurvas.adapter = adapter
        textWatcher(editFinca) { plano: PlanoSiembra, charSequence: CharSequence? ->
            plano.finca = charSequence.toString()
            setInvernadero(plano)
        }
        textWatcher(editInv) { plano: PlanoSiembra, charSequence: CharSequence? ->
            plano.inv = charSequence.toString()
            setInvernadero(plano)
        }
        textWatcher(editComun) { plano: PlanoSiembra, charSequence: CharSequence? ->
            plano.comun = charSequence.toString()
            setInvernadero(plano)
        }
        textWatcher(editLote) { plano: PlanoSiembra, charSequence: CharSequence? ->
            plano.lote = charSequence.toString()
            setInvernadero(plano)
        }
        textWatcher(editVariedad) { plano: PlanoSiembra, charSequence: CharSequence? ->
            plano.variedad = charSequence.toString()
            setInvernadero(plano)
        }
        textWatcher(editCalibre) { plano: PlanoSiembra, charSequence: CharSequence? ->
            plano.size = charSequence.toString()
            setInvernadero(plano)
        }
        btnFetch.setOnClickListener {
            mainViewModel.fetchPlano()
            mainViewModel.planos
        }

        mainViewModel.planos.observe(this@InvernaderoFragment, Observer { list ->
            if (list != null) {
                adapter.submitList(list)
            }
        })
    }

    private fun textWatcher(
        editText: EditText,
        function1: (PlanoSiembra, CharSequence?) -> Unit
    ) {
       val watcher= object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                val inv = mainViewModel.invernadero.value
                if (inv != null) {
                    function1(inv,s)
                }

            }

        }
        editText.addTextChangedListener(watcher)
    }

    private fun setInvernadero(inv: PlanoSiembra) {
        mainViewModel.setInvernadero(inv)
    }


}