package com.pyfflowers.muestreoft.ui.gallery

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.print.PrintHelper
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.pyfflowers.muestreoft.MainViewModel
import com.pyfflowers.muestreoft.R
import com.pyfflowers.muestreoft.adapters.MuestraAdapter
import com.pyfflowers.muestreoft.models.PlanoSiembra

class MuestrasActualesFragment : Fragment() {

    private lateinit var mainViewModel: MainViewModel
lateinit var recycler:RecyclerView
    lateinit var finca:EditText
    lateinit var invernadero:EditText
    lateinit var variedad:EditText
    lateinit var lote:EditText
    lateinit var calibre:EditText
    lateinit var btnSubirMuestras:Button
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mainViewModel =
            ViewModelProvider(this).get(MainViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_gallery, container, false)
        initializeComponents(root)
        return root

    }

    private fun initializeComponents(root: View) {
        val adapter=MuestraAdapter()
        with(root){
            recycler=findViewById(R.id.recycler_muestras)
            finca=findViewById(R.id.txt_finca)
            invernadero=findViewById(R.id.txt_invernadero)
            variedad=findViewById(R.id.txt_variedad)
            lote=findViewById(R.id.txt_lote)
            calibre=findViewById(R.id.txt_calibre)
            btnSubirMuestras=findViewById(R.id.btn_subir_muestras)
        }
        btnSubirMuestras.setOnClickListener {
            mainViewModel.subriMuestras()
        }
        recycler.layoutManager=GridLayoutManager(context,3)
        recycler.adapter=adapter
        textWatcher(finca){ planoSiembra, charSequence ->
            planoSiembra.finca=charSequence.toString()
            mainViewModel.setInvernadero(planoSiembra)
        }
        textWatcher(invernadero){ planoSiembra, charSequence ->
            planoSiembra.inv=charSequence.toString()
            mainViewModel.setInvernadero(planoSiembra)
        }
        textWatcher(variedad){ planoSiembra, charSequence ->
            planoSiembra.variedad=charSequence.toString()
            mainViewModel.setInvernadero(planoSiembra)
        }
        textWatcher(lote){ planoSiembra, charSequence ->
            planoSiembra.lote=charSequence.toString()
            mainViewModel.setInvernadero(planoSiembra)
        }
        textWatcher(calibre){ planoSiembra, charSequence ->
            planoSiembra.size=charSequence.toString()
            mainViewModel.setInvernadero(planoSiembra)
        }
        mainViewModel.totales.observe(this, Observer { muestras ->
            adapter.submitList(muestras)
        })
    }
    private fun textWatcher(
        editText: EditText,
        function1: (PlanoSiembra, CharSequence?) -> Unit
    ) {
        val watcher = object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                val inv=mainViewModel.invernadero.value
                if (inv != null) {
                    function1(inv, s)
                }

            }

        }
        editText.addTextChangedListener(watcher)
    }
}